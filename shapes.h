#ifndef _SHAPES_H_
#define _SHAPES_H_

#include <string>
using namespace std;

class Shape {
private:
  string name;
public:
  Shape(string name);
  virtual ~Shape();
  string description();
  virtual double area() = 0;
};

class Square : public Shape {
 private:
  double width;
 public:
  Square(double width);
  double area();
};

class Circle : public Shape {
	private:
	double radius;
	
	public:
	Circle(double radius); // Oh so its like funcname(int x , int y) <--
	double area();
};

class Rectangle : public Shape {
 private:
  double width;
  double height;
 public:
  Rectangle(double width , double height);
  double area();
};


#endif
