#include "shapes.h" // Adding the code.
#include <iostream>
using namespace std;
//using namespace Shape;

Shape::Shape(string name) 
{
  this->name = name; 
} 

Shape::~Shape() 
{
}

string Shape::description()
{
  return name + " has area: " + to_string(area());
}


	
Square::Square(double width) : Shape("Square")
{
	this->width = width; 
}

double Square::area()
{
	return width * width;
} 


